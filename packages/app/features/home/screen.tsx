import {
  Anchor,
  Button,
  H1,
  Input,
  Paragraph,
  Separator,
  Sheet,
  useToastController,
  XStack,
  YStack,
  Text

} from '@my/ui'
import { MyComponent,Header } from '@my/ui'
import { ChevronDown, ChevronUp } from '@tamagui/lucide-icons'
import { useState } from 'react'
import { useLink } from 'solito/link'

export function 
HomeScreen() {
  const linkProps = useLink({
    href: '/user/nate',
  })

const [item, setItem] = useState("");
const [itemsList, setItemsList] = useState<string[]>([]);

function handleAdd() {
  if (item.trim() !== "") {
    setItemsList([...itemsList, item]);
    setItem("");
  }
}

return (
  <YStack>
    <H1 textAlign='right'>Welcome to Tamagui.</H1>
    <XStack>
      <Input value={item} onChangeText={(text) => setItem(text)} />
      
      <Button onPress={handleAdd}></Button>
    </XStack>

    {itemsList.map((item, index) => (
      <Text key={index}>{item}</Text>
    ))}
  </YStack>
);

}



