import React from 'react';
import ReactDOM from 'react-dom';
import { XStack,YStack } from '@My/ui';
type AppProps = {
    name: string;
};
const App: React.FC<AppProps> = ({ name }) => {
    return (
        <YStack>
            <h1>Hello, {name}!</h1>
            <p>This is a <param name="varshini" value="20" />.</p>
        </YStack>
    );
};
ReactDOM.render(<App name="User" />, document.getElementById('root'));